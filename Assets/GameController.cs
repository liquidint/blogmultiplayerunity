﻿using UnityEngine;

using System;
using System.Collections;

using SocketIO;

public class GameController : MonoBehaviour {

	public GameObject PlayerBaseObject;
	public Plane GroundPlane;

	private GameObject _socketObject;
	private SocketIOComponent _socketComponent;

	// Game vars

	private int _maxVelocity = 10; // max translate distance per frame.
	private int _refresh = 150; // how often to get game state from the service in milliseconds.
	private float _coordScaleFactorX = 0.01f;
	private float _coordScaleFactorY = -0.01f;

	// Player data
	private JSONObject _playerData;

	// Who am I. GUID is private.
	private string _playerGuid = null;
	private int _playerID;

	// Where am I?
	private float _playerLocationX = 1;
	private float _playerLocationY = 1;

	// Where was I?
	private float _lastPlayerLocationX = -1;
	private float _lastPlayerLocationY = -1;

	// Sprite tracking
	private Hashtable _playerList = new Hashtable();
	private Hashtable _playerDirtyList = new Hashtable();

	// Where am I going?
	private float _playerTargetX = 1;
	private float _playerTargetY = 1;

	// Start the game.
	void Start () {

		// Hide our sample player object.
		SetVisibile (PlayerBaseObject, false);

		// Setup ground plane for mouse tracking.
		GroundPlane = new Plane(Vector3.up, transform.position);

		// Setup and connect the socket component.
		_socketObject = GameObject.Find("SocketIO");
		_socketComponent = _socketObject.GetComponent<SocketIOComponent>();

		_socketComponent.On("enteredGame", EnteredGame);
		_socketComponent.On("playerDisconnected", PlayerDisconnected);
		_socketComponent.On("playerChanged", PlayerChanged);

		_socketComponent.Connect ();

	}
	
	// Update is called once per frame.
	void Update () {

		// Capture and interpret mouse click.
		if (Input.GetButtonDown("Fire1")) {
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float distance;
			if (GroundPlane.Raycast(ray, out distance)){
				Vector3 hitPoint = ray.GetPoint(distance);
				SetTarget(hitPoint);
			}
		}

		// Update the local player on each frame.
		UpdateLocalPlayer ();
	}

	public void SetTarget(Vector3 mousePoint) {

		/*
     	* Navigate the sprite to a new place by setting the target coordinates. 
     	* The sprite will move to that position over time.
     	*/

		_playerTargetX = mousePoint.x / _coordScaleFactorX;
		_playerTargetY = mousePoint.z / _coordScaleFactorY;

	}

	public void EnteredGame(SocketIOEvent playerRef){
		Debug.Log(string.Format("EnteredGame: [name: {0}, data: {1}, sid: {2}]", playerRef.name, playerRef.data, _socketComponent.sid));

		// Set globals.
		_playerGuid = playerRef.data.GetField("GUID").str;
		_playerID = (int)playerRef.data.GetField ("id").n;
		_playerData = playerRef.data["playerList"];

		// Update game state.
		UpdateRemotePlayers();

		// Start sending player location.
		SendPlayerLocation();
	}

	// Called when another player has disconnected.
	public void PlayerDisconnected(SocketIOEvent playerRef){
		Debug.Log(string.Format("PlayerDisconnected: [name: {0}, data: {1}]", playerRef.name, playerRef.data));
		GameObject toBeDeleted = _playerList [Convert.ToInt32 (playerRef.data.GetField ("id").ToString ())] as GameObject;
		Destroy (toBeDeleted);
	}

	// Called when another players' properties have changed.
	public void PlayerChanged(SocketIOEvent playerRef){
		Debug.Log(string.Format("PlayerChanged: [name: {0}, data: {1}]", playerRef.name, playerRef.data));
		if (playerRef.data != null) {
			UpdateGameObject (playerRef.data);
		}
	}

	// Send this players' data.
	public void SendPlayerLocation() {
		// check for change before sending location data
		if ((_lastPlayerLocationX != _playerLocationX || _lastPlayerLocationY != _playerLocationY) && _socketComponent != null) {
			Debug.Log(string.Format("Sending: {0},{1}", _playerLocationX, _playerLocationY));
			JSONObject locPayload = new JSONObject();
			locPayload.AddField ("x", _playerLocationX);
			locPayload.AddField("y", _playerLocationY);
			_socketComponent.Emit("playerUpdate", locPayload);
			_lastPlayerLocationX = _playerLocationX;
			_lastPlayerLocationY = _playerLocationY;
		}
	}

	public void UpdateGameObject(JSONObject playerObj) {
		Debug.Log (string.Format ("UpdateGameObject"));

		/*
    	* Update the scene graph for the player 
    	*/

		GameObject pObj;

		int playerObjInt = (int)playerObj.GetField ("id").n;

		if (_playerList[playerObjInt] != null) {

			// get existing player sprite. using ID here because remote player GUID is not known.
			pObj = _playerList[playerObjInt] as GameObject;

		} else {

			Debug.Log(string.Format("Creating New Obj"));

			// player is new to the local player so create new sprite and add to stage.
			pObj = Instantiate(PlayerBaseObject);
			SetVisibile (pObj, true);

			_playerList[playerObjInt] = pObj;

			if (playerObjInt != _playerID) {
				// If this payer is not the local player then set the ball material to differentiate.
				// To set a material dynamically that has not been loaded by another object it must be stored under assets/resources.
				SetMaterial(pObj, "OtherPlayerMaterial");
			}

			// Set initial position - if not tweening
			//pObj.transform.localPosition.x = playerObj["LocationX"];
			//pObj.transform.localPosition.y = playerObj["LocationY"];

		}

		// Tween it for smooth motion
		LeanTween.move( pObj, new Vector3(
			float.Parse(playerObj["LocationX"].ToString()) * _coordScaleFactorX,
			0,
			float.Parse(playerObj["LocationY"].ToString()) * _coordScaleFactorY), 0.25f).setEase(LeanTweenType.linear);

		// or not to tween 
		/*
		pObj.transform.localPosition = new Vector3(
			float.Parse(playerObj["LocationX"].ToString()) * coordScaleFactorX,
			0,
			float.Parse(playerObj["LocationY"].ToString()) * coordScaleFactorY);
		*/

		// Set clean
		_playerDirtyList[playerObjInt] = 0;
	}


	public void UpdateLocalPlayer() {

		// Update my sprite
		float deltaX = _playerTargetX - _playerLocationX;
		float deltaY = _playerTargetY - _playerLocationY;

		if (Math.Abs(deltaX) > 0 || Math.Abs(deltaY) > 0) {
			float normalizedLength = _maxVelocity;
			float realLength = (float)(Math.Sqrt((deltaX * deltaX) + (deltaY * deltaY)));

			if (Math.Abs(realLength) > _maxVelocity) {
				deltaX = deltaX / realLength;
				deltaY = deltaY / realLength;

				deltaX = deltaX * normalizedLength;
				deltaY = deltaY * normalizedLength;

				_playerLocationX = _playerLocationX + deltaX;
				_playerLocationY = _playerLocationY + deltaY;

				/*
		     	* Send local player updates. Using frame to control rate of player updates.
		     	*/
				SendPlayerLocation ();

			}
		}
	}

	public void UpdateRemotePlayers() {

		/*
     	* Update all players with their new positions as received from 
     	* the service. The local player is also updated in this process.
     	*/

		if (_playerData != null) {

			ArrayList dirtyItems = new ArrayList(_playerDirtyList.Keys);

			// Assume all players are invalid until proven otherwise.
			foreach (object dirtyItemKey in dirtyItems) {
				_playerDirtyList[(int)dirtyItemKey] = 1;
			}

			// Update GameObjects.
			foreach (JSONObject playerItem in _playerData.list) {
				UpdateGameObject (playerItem);
			}

			// remove all dirty sprites. players that have left the game should be removed.
			foreach (object dirtyItemKey in dirtyItems) {
				if ((int)_playerDirtyList[(int)dirtyItemKey] == 1) {
					GameObject toBeDeleted = _playerList[(int)dirtyItemKey] as GameObject;
					Destroy (toBeDeleted);
				}
			}

		}
	}


	// Helper to set visbility of child renderers of a GameObject.
	public void SetVisibile(GameObject objectToSet, bool isVisible) {
		var renderers = objectToSet.GetComponentsInChildren<Renderer>();
		foreach (Renderer r in renderers) {
			r.enabled = isVisible;
		}
	}

	// Helper to set material of child renderers of a GameObject.
	public void SetMaterial(GameObject objectToSet, string materialName) {
		Material materialRes = Resources.Load(materialName, typeof(Material)) as Material;
		var renderers = objectToSet.GetComponentsInChildren<Renderer>();
		foreach (Renderer r in renderers) {
			r.material = materialRes;
		}
	}

}
